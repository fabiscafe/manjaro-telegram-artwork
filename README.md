# TelegramManjaro
Artwork and stuff for https://t.me/Manjaro

If you want to create artwork for the Group:

1. Fork this repo
2. Create and add your artwork to your fork
3. Do a pull request

for logos:
- [x] Flat design
- [x] Vectorgraphics only
- [x] Keep it simple
- [x] Manjaro Logo on it
- [x] Make sure that you agree with the [License](https://gitlab.com/Telegram-Manjaro/Artwork/-/blob/main/LICENSE)
- [x] ???
